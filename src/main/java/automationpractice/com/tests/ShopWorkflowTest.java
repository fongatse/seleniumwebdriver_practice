package automationpractice.com.tests;

import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import automationpractice.com.pageObject.Cart;
import automationpractice.com.pageObject.CartSummary;
import automationpractice.com.pageObject.Clothes;
import automationpractice.com.pageObject.ShoppingActions;
import automationpractice.com.pageObject.SignInForm;
import utils.DriverSettings;

public class ShopWorkflowTest extends DriverSettings {

    private Actions action;
    private Clothes clothes;
    private Cart cart;
    private ShoppingActions shoppingActions;
    private CartSummary summary;
    private SignInForm signinForm;

    @BeforeClass
    public void setup() {
        setDriver("http://automationpractice.com/index.php");
        action = new Actions(driver);
        cart = new Cart(driver);
        shoppingActions = new ShoppingActions(driver);
        summary = new CartSummary(driver);
        signinForm = new SignInForm(driver);
        clothes = new Clothes(driver, action, cart, shoppingActions, summary, signinForm, account);
    }

    @Test(priority = 1)
    public void selectClothes() {
        //1. dress
        clothes.goToDresses()
                .goToSummerDresses()
                .addSummerDressToCart(1);

        // 2. dress
        clothes.goToDresses()
                .goToCasualDresses()
                .addCasualDressToCart(1);

        // 3. dress
        clothes.goToDresses()
                .goToEveningDresses()
                .addEveningDressToCart(1);
    }

    @Test(priority = 2)
    public void deleteCartProducts() {
        clothes.verifyCartSize()
                .deleteCasualDressFromCart(2);
    }

    @Test(priority = 3)
    public void checkingCartProductsQtyAndPrice() {
        clothes.verifyCartSize()
                .goToDresses()
                .goToEveningDresses()
                .addEveningDressToCart(1);

        clothes.verifyDressTypeQty(1)
                .verifyDressTypeQty(2);

        clothes.verifyCartShippingCost("$2.00")
                .verifyCartTotalPrice("$132.96");

    }

    @Test(priority = 4)
    public void continueToShoppingSummary() {
        clothes.goToCheckout()
                .verifySummaryShippingPrice("$2.00")
                .verifySummaryProductsPrice("$130.96")
                .verifySummaryTotalPrice("$132.96");
    }

    @Test(priority = 5)
    public void increaseQtyOfProduct1() {
        clothes.verifySummaryShippingPrice("$2.00")
                .verifySummaryProductsPrice("$130.96")
                .verifySummaryTotalPrice("$132.96");

        //1st product increase
        clothes.increaseSummaryItemQty(1);

        clothes.verifySummaryShippingPrice("$2.00")
                .verifySummaryProductsPrice("$159.94")
                .verifySummaryTotalPrice("$161.94");
    }

    @Test(priority = 6)
    public void signInRequest() {
        clothes.summaryProceed()
                .signIn();
    }

    @Test(priority = 7)
    public void billingAndDeliveryAddress() {
        clothes.verifyBillingAdressName("Fidel Tesena")
                .verifyBillingAdressOne("Centar")
                .verifyBillingAdressCityState("Novi Sad, Connecticut 21000")
                .verifyBillingAdressCountry("United States")
                .verifyBillingAdressHomePhone("056")
                .verifyBillingAdressMobile("066");
    }

    @Test(priority = 8)
    public void termsOfServiceModal() {
        clothes.proceedToCheckout()
                .proceedToCheckout();

        clothes.verifyTermsServicesAlertModalDisplayed()
                .closeTermsServicesAlertModal();

        clothes.checkShippingTermsServices()
                .proceedToCheckout();
    }

    @Test(priority = 9)
    public void payment() {
        clothes.verifyBankWirePayment()
                .otherPaymentMethods()
                .verifyPayByCheckPayment();
    }

    @Test(priority = 10)
    public void confirmOrder() {
        clothes.confirmOrder();
    }

    @Test(priority = 11)
    public void checkIsOrderVisibleInOrderHistorySection() {
        clothes.goToAccountMenu()
                .accountOrderHistory();

        clothes.goToAccountMenu()
                .accountOrderHistory()
                .verifyOrderHistoryNotEmpty();

    }
}
