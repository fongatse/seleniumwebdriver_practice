package utils;

import automationpractice.com.pageObject.Account;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class DriverSettings {
    public WebDriver driver;
    public Account account;
    private String baseUrl;

    private void setBaseUrl(String url){ this.baseUrl = url;}

    public String getBaseUrl(){return this.baseUrl;}

    public void setDriver(String baseUrl) {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();
        this.driver.manage().window().maximize();
        account = new Account(driver);
        setBaseUrl(baseUrl);
        driver.get(getBaseUrl());
    }

    @AfterClass
    public void closeAll() {
        if(account.getAccountLogout().isDisplayed()){
        account.getAccountLogout().click();
        }
        driver.quit();
    }
}
