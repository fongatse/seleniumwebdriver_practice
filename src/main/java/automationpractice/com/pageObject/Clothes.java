package automationpractice.com.pageObject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import utils.EmailsGenerator;
import utils.Utils;

public class Clothes {

    private WebDriver driver;
    private Actions action;
    private Cart cart;
    private ShoppingActions shoppingActions;
    private CartSummary summary;
    private Account account;
    private SignInForm signinForm;
    int casuals;
    int summers;
    int evenings;
    int cartSize;


    public Clothes(WebDriver driver, Actions action, Cart cart, ShoppingActions shoppingActions, CartSummary summary, SignInForm signInForm, Account account) {
        this.driver = driver;
        this.action = action;
        this.cart = cart;
        this.shoppingActions = shoppingActions;
        this.summary = summary;
        this.signinForm = signInForm;
        this.account = account;

    }

    public WebElement getWomenBtn() {
        return Utils.waitToBeClickable(driver, By.xpath("//*[@id=\"block_top_menu\"]/ul/li[1]"), 30);
    }

    public WebElement getDressesBtn() {
        return Utils.waitToBeClickable(driver, By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]"), 30);
    }

    public WebElement getTShirtsBtn() {
        return Utils.waitToBeClickable(driver, By.xpath("//*[@id=\"block_top_menu\"]/ul/li[3]"), 30);
    }

    public WebElement getDressesDropDown() {
        return Utils.waitForElementPresence(driver, By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/ul"), 30);
    }

    public WebElement getCasualDressesBtn() {
        return Utils.waitToBeClickable(driver, By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]//a[contains(text(), \"Casual Dresses\")]"), 30);
    }

    public WebElement getEveningDressesBtn() {
        return Utils.waitToBeClickable(driver, By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]//a[contains(text(), \"Evening Dresses\")]"), 30);
    }

    public WebElement getSummerDressesBtn() {
        return Utils.waitToBeClickable(driver, By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]//a[contains(text(), \"Summer Dresses\")]"), 30);
    }

    /**
     * @param dressNum (value 1)
     */
    public WebElement getCasualDressProduct(int dressNum) {
        return Utils.waitForElementPresence(driver, By.xpath("//*[@id=\"center_column\"]/ul/li[" + dressNum + "]"), 30);
    }

    /**
     * @param dressNum (value 1)
     */
    public WebElement getEveningDressProduct(int dressNum) {
        return Utils.waitForElementPresence(driver, By.xpath("//*[@id=\"center_column\"]/ul/li[" + dressNum + "]"), 30);
    }

    /**
     * @param dressNum (values from 1 to 3)
     */
    public WebElement getSummerDressProduct(int dressNum) {
        return Utils.waitForElementPresence(driver, By.xpath("//*[@id=\"center_column\"]/ul/li[" + dressNum + "]"), 30);
    }

    public Clothes goToDresses() {
        if (getDressesBtn().isDisplayed()) {
            action.moveToElement(getDressesBtn()).perform();
        }
        return this;
    }

    private Clothes verifyClothesSubmenuDisplayed() {
        Assert.assertTrue(getSummerDressesBtn().isDisplayed());
        Assert.assertTrue(getCasualDressesBtn().isDisplayed());
        Assert.assertTrue(getEveningDressesBtn().isDisplayed());
        return this;
    }

    public Clothes verifyCartSize() {
        Assert.assertEquals(cart.getCartTotalQuantity().getText(), String.valueOf(this.cartSize));
        return this;
    }

    public void deleteCasualDressFromCart(int itemIndex) {

        try {
            action.moveToElement(cart.getCartTab()).perform();
            action.moveToElement(cart.getCartProductDeleteX(2)).perform();
            action.click(cart.getCartProductDeleteX(2)).build().perform();
            action.moveToElement(getDressesBtn()).perform();
            action.moveToElement(getEveningDressesBtn()).perform();
            getEveningDressesBtn().click();
            action.moveToElement(cart.getCartTab()).perform();
            this.cartSize--;
            this.casuals--;

            verifyCartSize();
        } catch (WebDriverException e) {
            System.out.println("index doesn't belong to an evening dress");
        }
    }

    public Clothes goToSummerDresses() {
        verifyClothesSubmenuDisplayed();
        action.moveToElement(getSummerDressesBtn()).perform();
        getSummerDressesBtn().click();
        verifySummerDressProducts();
        return this;
    }

    public Clothes goToEveningDresses() {
        Assert.assertTrue(getEveningDressesBtn().isDisplayed());

        action.moveToElement(getEveningDressesBtn()).perform();
        getEveningDressesBtn().click();
        return this;
    }

    private Clothes verifySummerDressProducts() {
        Assert.assertTrue(getSummerDressProduct(1).isDisplayed());
        Assert.assertTrue(getSummerDressProduct(2).isDisplayed());
        Assert.assertTrue(getSummerDressProduct(3).isDisplayed());
        Assert.assertEquals(getDressesCount().size(), 3);
        return this;
    }

    public Clothes addEveningDressToCart(int dressNum) {

        action.moveToElement(getEveningDressProduct(dressNum)).perform();
        action.moveToElement(shoppingActions.getAddToCartBtn()).perform();
        action.click(shoppingActions.getAddToCartBtn()).build().perform();

        Assert.assertTrue(shoppingActions.getAddToCartBtn().isDisplayed());

        action.click(shoppingActions.getContinueShopingBtn()).build().perform();
        action.moveToElement(cart.getCartTab()).perform();
        this.cartSize++;
        this.evenings++;

        verifyCartSize();
        return this;
    }

    public Clothes addSummerDressToCart(int dressNum) {
        action.moveToElement(getSummerDressProduct(dressNum)).perform();
        action.moveToElement(shoppingActions.getAddToCartBtn()).perform();

        Assert.assertTrue(shoppingActions.getAddToCartBtn().isDisplayed());

        action.click(shoppingActions.getAddToCartBtn()).build().perform();
        action.click(shoppingActions.getContinueShopingBtn()).build().perform();

        Assert.assertTrue(shoppingActions.getContinueShopingBtn().isDisplayed());

        action.moveToElement(cart.getCartTab()).perform();
        this.cartSize++;
        this.summers++;

        Assert.assertEquals(cart.getCartProductsQty().size(), this.cartSize);

        return this;
    }

    public Clothes addCasualDressToCart(int dressNum) {
        action.moveToElement(getCasualDressProduct(dressNum)).perform();
        action.moveToElement(shoppingActions.getAddToCartBtn()).perform();
        action.click(shoppingActions.getAddToCartBtn()).build().perform();

        Assert.assertTrue(shoppingActions.getAddToCartBtn().isDisplayed());

        action.click(shoppingActions.getContinueShopingBtn()).build().perform();
        action.moveToElement(cart.getCartTab()).perform();
        this.cartSize++;
        this.casuals++;

        Assert.assertEquals(cart.getCartProductsQty().size(), this.cartSize);
        return this;

    }

    public Clothes verifyDressTypeQty(int numOfProduct) {
        action.moveToElement(cart.getCartTab()).perform();
        action.moveToElement(cart.getCartProductsQty(numOfProduct)).perform();

        switch (numOfProduct) {
            case 1://summer dress
                Assert.assertEquals(cart.getCartProductsQty(1).getText(), String.valueOf(summers));
                break;

            case 2://evening dress
                Assert.assertEquals(cart.getCartProductsQty(2).getText(), String.valueOf(evenings));
                break;
        }
        return this;
    }

    public Clothes verifyCartShippingCost(String cost) {
        action.moveToElement(cart.getCartShipingCost()).perform();

        Assert.assertEquals(cart.getCartShipingCost().getText(), cost);
        return this;
    }

    public Clothes verifyCartTotalPrice(String price) {
        action.moveToElement(cart.getCartTotalPrice()).perform();

        Assert.assertEquals(cart.getCartTotalPrice().getText(), price);
        return this;
    }

    public Clothes goToCasualDresses() {
        action.moveToElement(getDressesBtn()).perform();

        Assert.assertTrue(getCasualDressesBtn().isDisplayed());

        action.moveToElement(getCasualDressesBtn()).perform();
        getCasualDressesBtn().click();
        return this;
    }

    public Clothes goToCheckout() {
        action.moveToElement(cart.getCartTab()).perform();
        action.moveToElement(cart.getCartTabCheckOutBtn()).perform();

        Assert.assertTrue(cart.getCartTabCheckOutBtn().isDisplayed());

        action.click(cart.getCartTabCheckOutBtn()).build().perform();
        return this;
    }

    public Clothes verifySummaryProductsPrice(String price) {
        Assert.assertTrue(summary.getCartSummaryTable().isDisplayed());
        Assert.assertEquals(summary.getCartSummTotalProductsPrice().getText(), price);
        return this;
    }

    public Clothes verifySummaryTotalPrice(String price) {
        Assert.assertTrue(summary.getCartSummaryTable().isDisplayed());
        Assert.assertEquals(summary.getCartSummaryTotalPrice().getText(), price);
        return this;
    }

    public Clothes verifySummaryShippingPrice(String price) {
        Assert.assertTrue(summary.getCartSummaryTable().isDisplayed());
        Assert.assertEquals(summary.getCartSummTotalShipping().getText(), price);
        return this;
    }

    public Clothes increaseSummaryItemQty(int itemNo) {
        summary.getCartSummQtyPlus(itemNo).click();
        driver.navigate().refresh();
        return this;
    }

    public Clothes summaryProceed() {
        summary.getCartProceedBtn().click();
        return this;
    }

    public Clothes signIn() {
        Assert.assertTrue(signinForm.getSignInForm().isDisplayed());

        signinForm.setEmailField(EmailsGenerator.getCurrentEmail());
        signinForm.setPasswordField("tester123");
        //goes to Billing Page
        signinForm.getSignInBtn().click();
        return this;
    }

    public Clothes verifyBillingAdressName(String name) {
        Assert.assertEquals(summary.getCartSummBillingAdressName().getText(), name);
        return this;
    }

    public Clothes verifyBillingAdressOne(String one) {
        Assert.assertEquals(summary.getCartSummBillingAdressOne().getText(), one);
        return this;
    }

    public Clothes verifyBillingAdressCityState(String cityState) {
        Assert.assertEquals(summary.getCartSummBillingAdressCityState().getText(), cityState);
        return this;
    }

    public Clothes verifyBillingAdressCountry(String country) {
        Assert.assertEquals(summary.getCartSummBillingAdressCountry().getText(), country);
        return this;
    }

    public Clothes verifyBillingAdressHomePhone(String homePhone) {
        Assert.assertEquals(summary.getCartSummBillingAdressHomePhone().getText(), homePhone);
        return this;
    }

    public Clothes verifyBillingAdressMobile(String mobile) {
        Assert.assertEquals(summary.getCartSummBillingAdressMobile().getText(), mobile);
        return this;
    }

    public Clothes proceedToCheckout() {
        summary.getCartProceedBtnTwo().click();
        return this;
    }

    public Clothes verifyTermsServicesAlertModalDisplayed() {
        action.moveToElement(summary.getCartSummTermsOfServiceDialog()).perform();
        Assert.assertTrue(summary.getCartSummTermsOfServiceDialog().isDisplayed());
        return this;

    }

    public Clothes closeTermsServicesAlertModal() {
        action.moveToElement(summary.getCartSummTermsOfServiceDialogClose()).perform();
        action.click(summary.getCartSummTermsOfServiceDialogClose()).build().perform();
        driver.navigate().refresh();
        return this;
    }

    public Clothes checkShippingTermsServices() {
        summary.getCartSummTermsOfServiceCheck().click();
        return this;
    }

    public Clothes verifyBankWirePayment() {
        summary.getCartSummPayByBankWire().click();
        Assert.assertEquals(summary.getCartSummPayByBankWireConfirm().getText(), "BANK-WIRE PAYMENT.");
        return this;
    }

    public Clothes verifyPayByCheckPayment() {
        summary.getCartSummPayByCheck().click();
        Assert.assertEquals(summary.getCartSummPayByCheckConfirm().getText(), "CHECK PAYMENT");
        return this;
    }

    public Clothes otherPaymentMethods() {
        summary.getCartSummOtherPaymentMethods().click();
        return this;
    }

    public Clothes confirmOrder() {
        summary.getCartSummConfirmOrderBtn().click();

        Assert.assertTrue(summary.getCartSummSuccessMsg().isDisplayed());
        Assert.assertEquals(summary.getCartSummSuccessMsg().getText(), "Your order on My Store is complete.");
        return this;
    }

    public Clothes goToAccountMenu() {
        //goes to AccountPage
        account.getAccountBtn().click();
        Assert.assertTrue(account.getAccountOrderHistoryBtn().isDisplayed());
        return this;
    }

    public Clothes accountOrderHistory() {
        //goes to account order history page
        account.getAccountOrderHistoryBtn().click();
        Assert.assertTrue(account.getAccountOrderListTable().isDisplayed());
        return this;
    }

    public Clothes verifyOrderHistoryNotEmpty() {
        Assert.assertNotEquals(account.getAccountOrdersLis().size(), null);
        return this;
    }

    public List<WebElement> getDressesCount() {
        return driver.findElements(By.xpath("//*[@id=\"center_column\"]/ul/li"));
    }

}
